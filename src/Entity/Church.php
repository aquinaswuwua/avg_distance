<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChurchRepository")
 */
class Church implements LatLngInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, nullable=true)
     */
    private $telephoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, nullable=true)
     */
    private $province;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=15, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=15, nullable=true)
     */
    private $northeastLat;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=15, nullable=true)
     */
    private $northeastLng;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=15, nullable=true)
     */
    private $southwestLat;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=15, nullable=true)
     */
    private $southwestLng;

    /**
     * @ORM\Column(type="string", length=350, nullable=true)
     */
    private $placeId;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=15, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name_ascii;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     */
    private $secret;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street): self
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelephoneNumber()
    {
        return $this->telephoneNumber;
    }

    /**
     * @param mixed $telephoneNumber
     */
    public function setTelephoneNumber($telephoneNumber): self
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     */
    public function setProvince($province): self
    {
        $this->province = $province;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return ViewportInterface
     */
    public function getNortheastLat()
    {
        return $this->northeastLat;
    }

    /**
     * @param mixed $northeastLat
     */
    public function setNortheastLat($northeastLat): self
    {
        $this->northeastLat = $northeastLat;

        return $this;
    }

    /**
     * @return ViewportInterface
     */
    public function getNortheastLng()
    {
        return $this->northeastLng;
    }

    /**
     * @param mixed $northeastLng
     */
    public function setNortheastLng($northeastLng): self
    {
        $this->northeastLng = $northeastLng;

        return $this;
    }

    /**
     * @return ViewportInterface
     */
    public function getSouthwestLat()
    {
        return $this->southwestLat;
    }

    /**
     * @param mixed $southwestLat
     */
    public function setSouthwestLat($southwestLat): self
    {
        $this->southwestLat = $southwestLat;

        return $this;
    }

    /**
     * @return ViewportInterface
     */
    public function getSouthwestLng()
    {
        return $this->southwestLng;
    }

    /**
     * @param mixed $southwestLng
     */
    public function setSouthwestLng($southwestLng): self
    {
        $this->southwestLng = $southwestLng;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }

    /**
     * @param mixed $placeId
     */
    public function setPlaceId($placeId): self
    {
        $this->placeId = $placeId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNameAscii()
    {
        return $this->name_ascii;
    }

    /**
     * @param mixed $name_ascii
     */
    public function setNameAscii($name_ascii)
    {
        $this->name_ascii = $name_ascii;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param $countyId
     *
     * @return Church
     */
    public function setCounty($county): self
    {
        $this->county = $county;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     */
    public function setSecret($secret): self
    {
        $this->secret = $secret;

        return $this;
    }
}
