<?php

namespace App\Entity;

interface LatLngInterface
{
    public function getLatitude(): ?string;

    public function getLongitude(): ?string;
}
