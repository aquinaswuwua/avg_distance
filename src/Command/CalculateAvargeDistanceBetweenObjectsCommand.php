<?php

namespace App\Command;

use App\Repository\ChurchRepository;
use App\Service\ArrAvgValueCalculate;
use App\Service\Observable\DistanceFromGivenObjectToClosestObjectObservable;
use App\Service\Observer\ClosestDistanceObserver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CalculateAvargeDistanceBetweenObjectsCommand extends Command
{
    protected static $defaultName = 'app:calculate-avarge-distance-between-objects';

    const INITIAL_DISTANCE_TO_CHECK_METERS = 1000;

    /**
     * @var ChurchRepository
     */
    private $churchRepository;

    /**
     * @var DistanceFromGivenObjectToClosestObjectObservable
     */
    private $closestObjectsObservable;

    /**
     * @var ClosestDistanceObserver
     */
    private $closestObjectsObserver;

    /**
     * @var ArrAvgValueCalculate
     */
    private $avgDisanceCalculator;

    /**
     * CalculateAvargeDistanceBetweenObjectsCommand constructor.
     *
     * @param ChurchRepository                                 $churchRepository
     * @param DistanceFromGivenObjectToClosestObjectObservable $closestObjectsObservable
     * @param ClosestDistanceObserver                          $closestObjectsObserver
     * @param ArrAvgValueCalculate                             $avgDisanceCalculator
     */
    public function __construct(
        ChurchRepository $churchRepository,
        DistanceFromGivenObjectToClosestObjectObservable $closestObjectsObservable,
        ClosestDistanceObserver $closestObjectsObserver,
        ArrAvgValueCalculate $avgDisanceCalculator
    ) {
        $this->churchRepository = $churchRepository;
        $this->closestObjectsObservable = $closestObjectsObservable;
        $this->closestObjectsObserver = $closestObjectsObserver;
        $this->avgDisanceCalculator = $avgDisanceCalculator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(
                'Given command counts avg closest distance between given objects'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->closestObjectsObservable->add($this->closestObjectsObserver);

        $churches = $this->churchRepository->findAll();

        foreach ($churches as $church) {
            $this->closestObjectsObservable->findClosestObjects(
                $church,
                self::INITIAL_DISTANCE_TO_CHECK_METERS
            );
        }

        $calculator = new ArrAvgValueCalculate($this->closestObjectsObserver->getClosestObjectsWithObject());

        $io->success(sprintf(
            'Done! The average distance between given objects is: %d',
            $calculator->calculateAvgDistance()
            ));
    }
}
