<?php

namespace App\Service\Observable;

use App\Entity\LatLngInterface;
use App\Service\ES\ClientES;
use App\Service\ES\QueryComposer\GeoDistanceQueryComposer;
use App\Service\Observer\ObserverInterface;

class DistanceFromGivenObjectToClosestObjectObservable implements ObservableInterface
{
    const DISTANCE_INCREASE_IF_NO_RESULT = 1000;

    /**
     * @var ClientES
     */
    private $clinetEs;

    /**
     * @var GeoDistanceQueryComposer
     */
    private $geoDistanceQueryComposer;

    private $result = [];

    private $observers = [];

    /**
     * DistanceFromGivenObjectToClosestObjectObservable constructor.
     *
     * @param ClientES $clinetEs
     * @param GeoDistanceQueryComposer $geoDistanceQueryComposer
     */
    public function __construct(
        ClientES $clinetEs,
        GeoDistanceQueryComposer $geoDistanceQueryComposer
    ) {
        $this->clinetEs = $clinetEs;
        $this->geoDistanceQueryComposer = $geoDistanceQueryComposer;
    }

    public function findClosestObjects(LatLngInterface $object, int $distance): bool
    {
        if (is_null($object->getLatitude()) || is_null($object->getLongitude())) {
            return false;
        }

        $query = $this->geoDistanceQueryComposer->getQuery($object, ['distance' => $distance]);

        $closestObjects = $this->clinetEs->getClient()->search($query);

        if (0 != $closestObjects['hits']['total']) {
            $this->result = $closestObjects['hits']['hits'][0]['sort'][0];

            $this->notify();

            return true;
        }

        $distance += self::DISTANCE_INCREASE_IF_NO_RESULT;

        $this->findClosestObjects($object, $distance);

        return false;
    }

    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update();
        }
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function add(ObserverInterface $observer): ObserverInterface
    {
        return $this->observers[] = $observer;
    }
}
