<?php


namespace App\Service\Observable;


use App\Service\Observer\ObserverInterface;

interface ObservableInterface
{

    public function add(ObserverInterface $object): ObserverInterface;


    public function notify();
}
