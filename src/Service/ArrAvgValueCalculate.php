<?php

namespace App\Service;

class ArrAvgValueCalculate
{
    /**
     * @var array
     */
    private $array;

    /**
     * ArrAvgValueCalculate constructor.
     *
     * @param array $array
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    public function calculateAvgDistance()
    {
        if (0 == count($this->array)) {
            throw new \DivisionByZeroError;
        }

        return array_sum($this->array) /
            count($this->array);
    }
}
