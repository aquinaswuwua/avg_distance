<?php

namespace App\Service\Observer;


use App\Service\Observable\DistanceFromGivenObjectToClosestObjectObservable;

class ClosestDistanceObserver implements ObserverInterface
{
    /**
     * @var DistanceFromGivenObjectToClosestObjectObservable
     */
    private $closestObjectsObservable;

    private $closestObjectsWithObject = [];

    /**
     * ClosestDistanceObserver constructor.
     *
     * @param DistanceFromGivenObjectToClosestObjectObservable $closestObjectsObservable
     */
    public function __construct(
        DistanceFromGivenObjectToClosestObjectObservable $closestObjectsObservable
    ) {
        $this->closestObjectsObservable = $closestObjectsObservable;
    }

    public function update()
    {
        $this->closestObjectsWithObject[] = $this->closestObjectsObservable->getResult();
    }

    public function getClosestObjectsWithObject(): array
    {
        return $this->closestObjectsWithObject;
    }
}
