<?php


namespace App\Service\Observer;

interface ObserverInterface
{
    public function update();
}