<?php

namespace App\Service\ES;


use Elasticsearch\ClientBuilder;

class ClientES
{

    private $hosts = [
            [
                'host' => 'localhost',
            ]
        ];

    private $client;


    public function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setHosts($this->hosts)
            ->build();
    }

    /**
     * @return \Elasticsearch\Client
     */
    public function getClient(): \Elasticsearch\Client
    {
        return $this->client;
    }
}

