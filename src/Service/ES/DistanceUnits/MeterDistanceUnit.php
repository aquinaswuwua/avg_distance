<?php

namespace App\Service\ES\DistanceUnits;

class MeterDistanceUnit
{
    private $unit = 'm';

    public function getUnit()
    {
        return $this->unit;
    }
}
