<?php

namespace App\Service\ES\QueryComposer;


use App\Entity\LatLngInterface;
use App\Service\ES\BodyQuery\GeoDistanceBodyQuery;
use App\Service\ES\Index\ChurchGeoIndex;
use App\Service\ES\Type\ChurchType;

class GeoDistanceQueryComposer
{

    /**
     * @var ChurchGeoIndex
     */
    private $index;

    /**
     * @var ChurchType
     */
    private $type;

    /**
     * @var GeoDistanceBodyQuery
     */
    private $body;

    /**
     * GeoDistanceQueryComposer constructor.
     *
     * @param ChurchGeoIndex $index
     * @param ChurchType $type
     * @param GeoDistanceBodyQuery $body
     */
    public function __construct(
        ChurchGeoIndex $index,
        ChurchType $type,
        GeoDistanceBodyQuery $body
    ) {
        $this->index = $index;
        $this->type = $type;
        $this->body = $body;
    }

    public function getQuery(LatLngInterface $object, array $distance): array
    {
        return $query = [
            'index' => $this->index->getIndex(),
            'type' => $this->type->getType(),
            'body' => $this->body->getBody($object, $distance),
        ];
    }
}
