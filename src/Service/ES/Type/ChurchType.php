<?php

namespace App\Service\ES\Type;

class ChurchType implements TypeInterface
{
    private $type = 'church';

    public function getType(): string
    {
        return $this->type;
    }
}
