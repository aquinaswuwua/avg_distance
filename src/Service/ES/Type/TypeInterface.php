<?php

namespace App\Service\ES\Type;


interface TypeInterface
{
    public function getType(): string;
}