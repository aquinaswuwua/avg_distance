<?php

namespace App\Service\ES\Index;

class ChurchGeoIndex implements IndexInterface
{
    private $index = 'churches_geo';

    public function getIndex(): string
    {
        return $this->index;
    }
}
