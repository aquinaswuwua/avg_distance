<?php

namespace App\Service\ES\Index;


interface IndexInterface
{
    public function getIndex(): string;
}