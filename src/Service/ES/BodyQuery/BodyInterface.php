<?php

namespace App\Service\ES\BodyQuery;

use App\Entity\LatLngInterface;

interface BodyInterface
{
    public function getBody(LatLngInterface $object, array $param): array;
}
