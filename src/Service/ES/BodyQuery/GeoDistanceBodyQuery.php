<?php

namespace App\Service\ES\BodyQuery;

use App\Entity\LatLngInterface;
use App\Service\ES\DistanceUnits\MeterDistanceUnit;

class GeoDistanceBodyQuery implements BodyInterface
{
    /**
     * @var MeterDistanceUnit
     */
    private $meterDistanceUnit;

    /**
     * GeoDistanceBodyQuery constructor.
     *
     * @param MeterDistanceUnit $meterDistanceUnit
     */
    public function __construct(MeterDistanceUnit $meterDistanceUnit)
    {
        $this->meterDistanceUnit = $meterDistanceUnit;
    }

    public function getBody(LatLngInterface $object, array $params): array
    {
        if (!$object instanceof LatLngInterface) {
            return [];
        }

        return [
            'query' => [
                'bool' => [
                    'must' => [
                        'match_all' => new \stdClass()
                    ],
                    'must_not' => [
                        'term' => [
                            'id' => $object->getId()
                        ]
                    ],
                    'filter' => [
                        'geo_distance' => [
                            'distance' => sprintf('%d%s',
                                $params['distance'],
                                $this->meterDistanceUnit->getUnit()
                            ),
                            'lat_lang' => [
                                'lat' => $object->getLatitude(),
                                'lon' => $object->getLongitude()
                            ]
                        ]
                    ]
                ]
            ],
            'sort' => [
                '_geo_distance' => [
                    'lat_lang' => [
                        'lat' => $object->getLatitude(),
                        'lon' => $object->getLongitude()
                    ],
                    'order' => 'asc'
                ]
            ]
        ];
    }
}
